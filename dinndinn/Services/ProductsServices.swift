//
//  ProductsServices.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import Foundation
import Moya


enum ProductsService {
    case PolpularProducts
}

extension ProductsService: TargetType, AccessTokenAuthorizable {
    
    
    var baseURL: URL { return URL(string: NetworkConstants.baseURL)! }
    
    var path: String {
        switch self {
        case .PolpularProducts:
            return NetworkConstants.popularArticles
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .PolpularProducts:
            return Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .PolpularProducts:
            return .requestParameters(parameters: [NetworkConstants.ParametersNames.apiKey: NetworkConstants.apiKey], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        [:]
    }
    
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .PolpularProducts:
            return .none
        }
    }
}
