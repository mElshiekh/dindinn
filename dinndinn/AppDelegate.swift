//
//  AppDelegate.swift
//  dinndinn
//
//  Created by Elshiekh on 6/4/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setRoot()
        return true
    }

    func setWindow() {
        window = UIWindow()
        window?.makeKeyAndVisible()
    }

    func setRoot() {
        setWindow()
        let router = ProductsRouter()
        router.start()
        let homeVC = router.entryPoint
        window?.rootViewController = homeVC
    }
}
