//
//  ProductsResponse.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import ObjectMapper

class ProductsResponse: Mappable {
    var results: [FoodItem]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        results <- map["results"]
    }
}
