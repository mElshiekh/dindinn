//
//  NetworkError.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import Foundation
struct NetWorkError: Error {
    var message: String
}
