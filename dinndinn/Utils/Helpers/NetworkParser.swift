//
//  NetworkParser.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import ObjectMapper
import RxCocoa
import RxSwift

class NetworkParser {
    static let shared = NetworkParser()
    private init() {}

    func parseResponse<T>(data: Data?, returnType: T.Type, objResponse: PublishSubject<T?>) where T: Mappable {
        do {
            if let _ = data, let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] {
                let response = T(JSON: dict)
                objResponse.onNext(response)
                objResponse.onCompleted()
            }
        } catch {
            handleError(objResponse: objResponse)
        }
    }

    func handleError<T>(objResponse: PublishSubject<T?>) {
        objResponse.onNext(nil)
        objResponse.onError(NetWorkError(message: "Server error"))
        objResponse.onCompleted()
    }
}
