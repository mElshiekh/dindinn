//
//  Constants.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import Foundation

class NetworkConstants {
    static var apiKey = "yPzGTgz1HF1ehacnpbV5Tich4MxuRAV9"
    static var baseURL = "https://api.nytimes.com/"
    static var popularArticles = "svc/mostpopular/v2/viewed/1.json"
    
    class ParametersNames {
        static var apiKey = "api-key"
    }
}
