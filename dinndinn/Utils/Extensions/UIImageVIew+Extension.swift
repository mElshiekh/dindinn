//
//  UIImageVIew+Extension.swift
//  dinndinn
//
//  Created by Elshiekh on 6/12/21.
//

import Kingfisher
import UIKit

extension UIImageView {
    func setImageWith(urlString: String, placeholder: UIImage? = nil, indicator: IndicatorType = .activity, completion: ((_: UIImage?) -> Void)? = nil) {
        DispatchQueue.main.async { [weak self] in
            if let url = URL(string: urlString) {
                self?.kf.indicatorType = indicator
                self?.kf.setImage(
                    with: url,
                    placeholder: placeholder,
                    options: [
                    ], completionHandler:  {
                        result in
                        switch result {
                        case .success:
                            completion?(self?.image)
                        case .failure:
                            if placeholder == nil {
                                self?.setDefaultImage()
                            } else {
                                self?.image = placeholder
                            }
                            break
                        }
                    })
            } else {
                if placeholder == nil {
                    self?.setDefaultImage()
                } else {
                    self?.image = placeholder
                }
            }
        }
    }

    private func setDefaultImage() {
        image = UIImage(color: #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1), size: bounds.size)
    }
}
