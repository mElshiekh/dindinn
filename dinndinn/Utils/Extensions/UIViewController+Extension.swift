//
//  UIViewController+Extension.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import UIKit
extension UIViewController {
    func instantiateViewController(viewControllerId: String, StoryboardId: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: StoryboardId, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        return controller
    }

    class func instantiateViewController(viewControllerId: String, StoryboardId: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: StoryboardId, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        return controller
    }

    func embed(_ viewControllers: [UIViewController], inParent controller: UIViewController, inView view: UIView) {
        self.view.layoutIfNeeded()
        let width = view.bounds.width
        NSLayoutConstraint.activate([
            view.widthAnchor.constraint(equalToConstant: CGFloat(viewControllers.count) * width),
        ])
        view.layoutIfNeeded()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            for (index, viewController) in viewControllers.enumerated() {
                viewController.willMove(toParent: controller)
                let width = width
                viewController.view.frame = CGRect(x: CGFloat(index) * width, y: 0, width: width, height: view.bounds.height)
                view.addSubview(viewController.view)
                controller.addChild(viewController)
                viewController.didMove(toParent: controller)
                viewController.view.widthAnchor.constraint(equalToConstant: width).isActive = true
            }
        }
        self.view.layoutIfNeeded()
    }
}
