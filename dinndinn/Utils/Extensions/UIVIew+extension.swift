//
//  UIVIew+extension.swift
//  dinndinn
//
//  Created by Elshiekh on 6/7/21.
//

import UIKit

extension UIView {
    func xibSetUp(view: inout UIView?, nibName: String) {
        view = loadViewFromNib(nibName: nibName)
        view!.frame = bounds
        view!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view!)
        layoutIfNeeded()
    }

    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }

    func loadViewFromNib(nibName: String) -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        backgroundColor = .clear
        return view
    }

    func round(corners: UIRectCorner, radius: CGFloat) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }

    func fillSuperView(shouldUseSafeArea: Bool = true, padding: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.leadingAnchor :
                superview.leadingAnchor, constant: padding.left),
            trailingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.trailingAnchor :
                superview.trailingAnchor, constant: -padding.right),

            topAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.topAnchor :
                superview.topAnchor, constant: padding.top),
            bottomAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.bottomAnchor :
                superview.bottomAnchor, constant: -padding.bottom),
        ])
    }

    func fillSuperViewVertically(shouldUseSafeArea: Bool = true, padding: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.topAnchor :
                superview.topAnchor, constant: padding.top),
            bottomAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.bottomAnchor :
                superview.bottomAnchor, constant: -padding.bottom),
        ])
    }

    func fillSuperViewHorizontally(shouldUseSafeArea: Bool = true, padding: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.leadingAnchor :
                superview.leadingAnchor, constant: padding.left),
            trailingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.trailingAnchor :
                superview.trailingAnchor, constant: -padding.right),
        ])
    }

    func embed(_ views: [UIView]) {
        layoutIfNeeded()
        let initialWidth = bounds.width
        layoutIfNeeded()
        DispatchQueue.main.async { [weak self] in
            self?.widthAnchor.constraint(equalToConstant: CGFloat(views.count) * initialWidth).isActive = true
        }
        for (index, view) in views.enumerated() {
            let width = initialWidth
            view.frame = CGRect(x: CGFloat(index) * width, y: 0, width: width, height: bounds.height)
            addSubview(view)
            view.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    }

    func removeAllConstraints() {
        removeConstraints(constraints)
    }

    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = transform.rotated(by: radians)
        transform = rotation
    }
}

@IBDesignable
extension UIView {
    @IBInspectable var isHalfRounded: Bool {
        get {
            return layer.cornerRadius == layer.bounds.size.height / 2
        } set {
            clipsToBounds = true
            layer.cornerRadius = layer.bounds.size.height / 2
        }
    }

    // MARK: - Adding MasksToBounds

    @IBInspectable var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        } set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
        } set {
            layer.borderColor = newValue.cgColor
        }
    }

    @IBInspectable internal var shadowColor: UIColor {
        get {
            return UIColor(cgColor: layer.shadowColor ?? UIColor.clear.cgColor)
        } set {
            layer.shadowColor = newValue.cgColor
        }
    }

    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        } set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        } set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable var shadowOffsetX: CGFloat {
        get {
            return layer.shadowOffset.width
        } set {
            layer.shadowOffset = CGSize(width: newValue, height: layer.shadowOffset.height)
        }
    }

    @IBInspectable var shadowOffsetY: CGFloat {
        get {
            return layer.shadowOffset.height
        } set {
            layer.shadowOffset = CGSize(width: layer.shadowOffset.width, height: newValue)
        }
    }
}
