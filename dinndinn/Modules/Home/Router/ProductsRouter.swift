//
//  ProductsRouter.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import UIKit
typealias EntryPoint = AnyProductsView & UIViewController

class ProductsRouter: AnyProductsRouter {
    var entryPoint: EntryPoint?
    func start() -> AnyProductsRouter {
        let router = ProductsRouter()
        let view = UIViewController.instantiateViewController(viewControllerId: "ProductsViewController", StoryboardId: "Products") as! AnyProductsView
        entryPoint = view as? EntryPoint
        let presenter: AnyProductsPresenter = ProductsPresenter()
        let interactor: AnyProductsInteractor = ProductsInteractor()
        view.presenter = presenter
        interactor.presenter = presenter
        presenter.router = router
        presenter.view = view
        presenter.interactor = interactor
        presenter.initialize()
        return router
    }
}
