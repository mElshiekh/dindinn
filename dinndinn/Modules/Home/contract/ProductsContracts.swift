//
//  ProductsContracts.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import RxSwift
import RxCocoa

protocol AnyProductsRouter {
    func start() -> AnyProductsRouter
}

protocol AnyProductsPresenter: class {
    var router: AnyProductsRouter? { get set }
    var interactor: AnyProductsInteractor? { get set }
    var view: AnyProductsView? { get set }
    var categoriesViewData: BehaviorRelay<[FoodCategory]> { get set }
    var productsViewData: [FoodCategory: BehaviorRelay<[FoodItem]>] { get set }
    var errorViewData: BehaviorRelay<NetWorkError?> { get set }
    var isLoadingViewData: BehaviorRelay<Bool?> { get set }
    func initialize()
    func getProducts()
}

protocol AnyProductsInteractor: class {
    var view: AnyProductsView? { get set }
    var productsViewData: PublishSubject<ProductsResponse?> { get set }
    var presenter: AnyProductsPresenter? { get set }
    func getProducts()
}

protocol AnyProductsView: class {
    var presenter: AnyProductsPresenter? { get set }
}
