//
//  ProductsPresenter.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import RxCocoa
import RxSwift

class ProductsPresenter: AnyProductsPresenter {
    var productsViewData = [FoodCategory: BehaviorRelay<[FoodItem]>]()
    var categoriesViewData = BehaviorRelay<[FoodCategory]>(value: [])
    var errorViewData = BehaviorRelay<NetWorkError?>(value: nil)
    var isLoadingViewData = BehaviorRelay<Bool?>(value: nil)
    private var disposeBag = DisposeBag()
    var router: AnyProductsRouter?
    var interactor: AnyProductsInteractor?
    var view: AnyProductsView?

    func initialize() {
        interactor?.productsViewData.asObservable().subscribe(onNext: { [weak self] response in
            self?.isLoadingViewData.accept(false)
            if let resp = response?.results {
                var dict = [String: [FoodItem]]()
                for item in resp {
                    if dict[item.section ?? ""] == nil {
                        dict[item.section ?? ""] = [FoodItem]()
                    }
                    dict[item.section ?? ""]?.append(item)
                }
                let categs = dict.keys.map { (item) -> FoodCategory in
                    let catItem = FoodCategory(foodItems: dict[item] ?? [], name: item)
                    self?.productsViewData[catItem] = BehaviorRelay<[FoodItem]>(value: dict[item] ?? [])
                    return catItem
                }
                categs.first?.isSelected = true
                self?.categoriesViewData.accept(categs)
            }
        }, onError: { [weak self] respError in
            if let error = respError as? NetWorkError {
                self?.errorViewData.accept(error)
                self?.isLoadingViewData.accept(false)
            }
        }).disposed(by: disposeBag)
    }

    func getProducts() {
        interactor?.getProducts()
    }
}
