//
//  ProductsViewController.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import UIKit
import RxSwift

class ProductsViewController: UIViewController, AnyProductsView {
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var scrollInnerView: UIView!
    @IBOutlet var pageController: UIPageControl!
    @IBOutlet var cartButtonView: UIView!
    
    var presenter: AnyProductsPresenter?
    var disposeBag = DisposeBag()

    let dotImage: UIImage = {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        v.backgroundColor = .white
        v.cornerRadius = 5
        let i = v.asImage()
        return i
    }()
    
    let currentDotImage: UIImage = {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        v.backgroundColor = .white
        v.cornerRadius = 6
        let i = v.asImage()
        return i
    }()

    weak var pullUpViewController: FoodParentViewController?

    var images = ["https://images.unsplash.com/photo-1590767187868-b8e9ece0974b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHx8&w=1000&q=80", "https://www.filmibeat.com/ph-big/2020/02/v-2020_158253142110.jpg", "https://images.pexels.com/photos/1770809/pexels-photo-1770809.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"]

    override func viewDidLoad() {
        super.viewDidLoad()
        addPullUpController(animated: true)
        setupScrollView()
        setupPageController()
        view.bringSubviewToFront(cartButtonView)
        presenter?.getProducts()
    }
    @IBAction func goToCart(_ sender: UIButton) {
        print("Go to cart")
    }

    private func setupPageController() {
        pageController.numberOfPages = images.count
        for i in 0 ..< images.count {
            pageController.setIndicatorImage(dotImage, forPage: i)
        }
        pageController.setIndicatorImage(currentDotImage, forPage: 0)
    }

    private func setupScrollView() {
        scrollView.delegate = self
        let imageViews = images.map({ url -> UIImageView in
            let i = UIImageView()
            i.setImageWith(urlString: url)
            i.contentMode = .scaleAspectFill
            return i
        })
        scrollInnerView.embed(imageViews)
    }
}

extension ProductsViewController: PullUpViewControllerDelegate {
    private func addPullUpController(animated: Bool) {
        let pullUpController = createPullUpViewControllerIfNeeded()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: animated)
    }

    private func createPullUpViewControllerIfNeeded() -> FoodParentViewController {
        let currentPullUpController = children
            .filter({ $0 is FoodParentViewController })
            .first as? FoodParentViewController
        pullUpViewController = currentPullUpController ?? storyboard?.instantiateViewController(identifier: "FoodParentViewController")
        pullUpViewController?.delegate = self
        pullUpViewController?.presenter = presenter
        pullUpViewController?.screenState = .contracted
        return pullUpViewController!
    }

    func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        if stickyPoint >= pullUpViewController?.expandedSize ?? 0 {
            scrollView.alpha = 0
        } else {
            scrollView.alpha = 1
        }
    }
}

extension ProductsViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageOffset = scrollView.contentOffset.x / scrollView.bounds.width
        let index = Int(pageOffset)
        pageController.currentPage = index
        if let items = pageController.subviews.first?.subviews.first?.subviews {
            for (i, _) in items.enumerated() {
                if i == index {
                    pageController.setIndicatorImage(currentDotImage, forPage: i)
                } else {
                    pageController.setIndicatorImage(dotImage, forPage: i)
                }
            }
        }
    }
}
