//
//  ProductTableViewCell.swift
//  dinndinn
//
//  Created by Elshiekh on 6/13/21.
//

import UIKit
protocol ProductTableViewCellDelegate: class {
    func addToCart(cell: ProductTableViewCell)
}

class ProductTableViewCell: UITableViewCell {
    static let identifier = "ProductTableViewCell"
    
    weak var delegate: ProductTableViewCellDelegate?

    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var weightSizeLabel: UILabel!
    @IBOutlet var addToCartButton: UIButton!
    
    @IBAction func addToCart(_ sender: UIButton) {
        delegate?.addToCart(cell: self)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        self.backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    func setup(image: String, name: String, description: String, weightAndSize: String, price: String, delegate: ProductTableViewCellDelegate) {
        productImageView?.setImageWith(urlString: image)
        nameLabel.text = name
        descriptionLabel.text = description
        weightSizeLabel.text = weightAndSize
        addToCartButton.setTitle(price, for: .normal)
        self.delegate = delegate
    }
    
    
    @objc private func editProductAction() {
        delegate?.addToCart(cell: self)
    }
}
