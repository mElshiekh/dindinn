//
//  CategoryItemCollectionViewCell.swift
//  dinndinn
//
//  Created by Elshiekh on 6/8/21.
//

import UIKit

class CategoryItemCollectionViewCell: UICollectionViewCell {
    static let identifier = "CategoryItemCollectionViewCell"
    
    var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        l.textColor = .black
        l.lineBreakMode = .byWordWrapping
        return l
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit() {
        self.backgroundColor = UIColor.clear
        contentView.addSubview(titleLabel)
        titleLabel.fillSuperView(shouldUseSafeArea: false, padding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }

    func setupWith(title: String, isSelected: Bool) {
        titleLabel.text = title
        titleLabel.textColor = isSelected ? UIColor.black : UIColor.darkGray
    }
}

