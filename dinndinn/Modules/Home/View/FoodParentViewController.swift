//
//  FoodParentViewController.swift
//  dinndinn
//
//  Created by Elshiekh on 6/12/21.
//

import PullUpController
import RxCocoa
import RxSwift

protocol PullUpViewControllerDelegate: class {
    func pullUpControllerDidMove(to stickyPoint: CGFloat)
}

class FoodParentViewController: PullUpController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var holdingView: UIView!
    @IBOutlet var scrollView: UIScrollView!

    var presenter: AnyProductsPresenter?
    var disposeBag = DisposeBag()
    var items: [FoodCategory] {
        return presenter?.categoriesViewData.value ?? []
    }

    enum ScreenState {
        case contracted
        case expanded
    }

    lazy var contractedSize = (parent?.view.bounds.height ?? UIScreen.main.bounds.height) - 400 + 15
    lazy var expandedSize = (parent?.view.bounds.height ?? UIScreen.main.bounds.height)

    weak var delegate: PullUpViewControllerDelegate?

    var screenState: ScreenState = .contracted

    var initialPointOffset: CGFloat {
        switch screenState {
        case .contracted:
            return contractedSize
        case .expanded:
            return expandedSize
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        items.first?.isSelected = true
        setupCollectionView()
        scrollView.delegate = self
        bindPresenter()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.cornerRadius = 15
    }

    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch screenState {
        case .contracted:
            return [contractedSize]
        case .expanded:
            return [expandedSize]
        }
    }

    override func pullUpControllerDidDrag(to point: CGFloat) {
        print("pullUpControllerDidDrag" + "\(point)")
        delegate?.pullUpControllerDidMove(to: point)
    }

    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: expandedSize)
    }

    override var pullUpControllerBounceOffset: CGFloat {
        return 20
    }

    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }
}

extension FoodParentViewController {
    private func setupCollectionView() {
        collectionView.register(CategoryItemCollectionViewCell.self, forCellWithReuseIdentifier: CategoryItemCollectionViewCell.identifier)
    }

    private func setupChildrenViewControllers() {
        var vcs: [UIViewController] = []
        for item in items {
            if let vc = storyboard?.instantiateViewController(identifier: "FoodListViewController") as? FoodListViewController {
                vc.listKey = item
                vc.presenter = presenter
                vcs.append(vc)
            }
        }
        for subview in holdingView.subviews {
            subview.removeFromSuperview()
            subview.isHidden = true
        }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if vcs.count > 0 {
                self.embed(vcs, inParent: self, inView: self.holdingView)
            } else {
                NSLayoutConstraint.activate([
                    self.holdingView.widthAnchor.constraint(equalToConstant: self.scrollView.bounds.width),
                ])
            }
        }
    }

    private func selectCollectionViewItem(index: Int) {
        for item in items {
            item.isSelected = false
        }
        items[index].isSelected = true
        collectionView.reloadData()
        let indexPath = IndexPath(item: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
    }
}

extension FoodParentViewController {
    private func bindPresenter() {
        if let presenter = presenter {
            presenter.categoriesViewData.asObservable().subscribe(onNext: { [weak self] data in
                if data != nil {
                    self?.setupChildrenViewControllers()
                }
            }).disposed(by: disposeBag)

            presenter.categoriesViewData
                .bind(to: collectionView.rx.items(cellIdentifier: CategoryItemCollectionViewCell.identifier, cellType: CategoryItemCollectionViewCell.self)) { _, item, cell in
                    cell.setupWith(title: item.name ?? "", isSelected: item.isSelected)
                }.disposed(by: disposeBag)
        }
        collectionView.rx.modelSelected(FoodCategory.self).subscribe(onNext: { [weak self] category in
            guard let self = self else { return }
            if let index = self.items.firstIndex(of: category) {
                self.selectCollectionViewItem(index: index)
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.bounds.width * CGFloat(index), y: 0), animated: true)
            }
        })
    }
}

extension FoodParentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        view.layoutIfNeeded()
        let width = NSString(string: items[indexPath.item].name ?? "").size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .bold)]).width + 20
        return CGSize(width: width, height: collectionView.bounds.size.width)
    }
}

extension FoodParentViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            let pageOffset = scrollView.contentOffset.x / scrollView.bounds.width
            let index = Int(pageOffset)
            selectCollectionViewItem(index: index)
        }
    }
}
