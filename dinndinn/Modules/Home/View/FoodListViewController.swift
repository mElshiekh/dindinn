//
//  FoodListViewController.swift
//  dinndinn
//
//  Created by Elshiekh on 6/12/21.
//

import RxCocoa
import RxSwift

class FoodListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!

    var presenter: AnyProductsPresenter?
    var disposeBag = DisposeBag()

    var listKey: FoodCategory?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindPresenter()
    }
}

extension FoodListViewController {
    private func setupTableView() {
        tableView.register(UINib(nibName: ProductTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ProductTableViewCell.identifier)
    }
}

extension FoodListViewController {
    private func bindPresenter() {
        if let presenter = presenter, let listKey = listKey {
            presenter.productsViewData[listKey]?
                .bind(to: tableView.rx.items(cellIdentifier: ProductTableViewCell.identifier, cellType: ProductTableViewCell.self)) { [weak self] _, element, cell in
                        guard let self = self else { return }
                        cell.setup(image: element.media?.first?.mediaMetadata?.last?.url ?? "", name: element.title ?? "", description: element.abstract ?? "", weightAndSize: element.publishedDate ?? "", price: "\(element.id ?? 0)", delegate: self)
                }.disposed(by: disposeBag)
        }
    }
}

extension FoodListViewController: ProductTableViewCellDelegate {
    func addToCart(cell: ProductTableViewCell) {
        if let index = tableView.indexPath(for: cell)?.item {
            print(index)
        }
    }
}
