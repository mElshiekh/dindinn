//
//  TabsTableViewCell.swift
//  dinndinn
//
//  Created by Elshiekh on 6/7/21.
//

import UIKit

class TabsTableViewCell: UITableViewHeaderFooterView {
    static let identifier = "TabsTableViewCell"


    var onSelectItemAt: ((_: Int) -> Void)?
    var items: [FoodCategory]?

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .clear
        tintColor = .clear
    }

    override func didMoveToSuperview() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func setup(withItems items: [FoodCategory], onSelectItemAt: @escaping (_: Int) -> Void) {
        self.items = items
        self.onSelectItemAt = onSelectItemAt
        collectionView.reloadData()
    }
}

extension TabsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryItemCollectionViewCell.identifier, for: indexPath) as! CategoryItemCollectionViewCell
        let item = items?[indexPath.item]
        cell.setupWith(title: item?.name ?? "", isSelected: item?.isSelected ?? false)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for var item in items ?? [] {
            item.isSelected = false
        }
        items?[indexPath.item].isSelected = true
        collectionView.reloadData()
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        onSelectItemAt?(indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = NSString(string: items?[indexPath.item].name ?? "").size(withAttributes: [NSAttributedString.Key.font:
                UIFont.systemFont(ofSize: 16, weight: .bold)]).width + 20
        return CGSize(width: width, height: collectionView.bounds.size.width)
    }
}
