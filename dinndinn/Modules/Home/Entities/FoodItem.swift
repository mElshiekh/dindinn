//
//  FoodItem.swift
//  dinndinn
//
//  Created by Elshiekh on 6/8/21.
//

import ObjectMapper

// MARK: - Article

class FoodItem: NSObject, Mappable {
    var uri: String?
    var url: String?
    var id, assetID: Int?
    var publishedDate, section, title: String?
    var media: [Media]?
    var abstract: String?

    required init?(map: Map) {}

    func mapping(map: Map) {
        uri <- map["uri"]
        url <- map["url"]
        id <- map["id"]
        assetID <- map["asset_id"]
        publishedDate <- map["published_date"]
        section <- map["section"]
        title <- map["title"]
        media <- map["media"]
        abstract <- map["abstract"]
    }
}

// MARK: - Media

class Media: Mappable {
    var mediaMetadata: [MediaMetadatum]?

    required init?(map: Map) {}
    func mapping(map: Map) {
        mediaMetadata <- map["media-metadata"]
    }
}

// MARK: - MediaMetadatum

class MediaMetadatum: Mappable {
    var url: String?

    required init?(map: Map) {}

    func mapping(map: Map) {
        url <- map["url"]
    }
}
