//
//  FoodCategory.swift
//  dinndinn
//
//  Created by Elshiekh on 6/8/21.
//

import Foundation
import ObjectMapper

class FoodCategory: NSObject, Mappable {
    var id: String?
    var name: String?
    var subItems: [FoodItem]?
    var isSelected = false
    init(foodItems: [FoodItem], name: String) {
        self.subItems = foodItems
        self.name = name
        self.id = name
    }
    required init?(map: Map) { }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        subItems <- map["subItems"]
    }
}
