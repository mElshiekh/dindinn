//
//  ProductsInteractor.swift
//  dinndinn
//
//  Created by Elshiekh on 6/6/21.
//

import Moya
import RxCocoa
import RxSwift

class ProductsInteractor: AnyProductsInteractor {
    var presenter: AnyProductsPresenter?
    
    private var disposeBag = DisposeBag()
    private var provider = MoyaProvider<ProductsService>()
    var productsViewData = PublishSubject<ProductsResponse?>()

    var view: AnyProductsView?

    func getProducts() {
        provider.rx.request(.PolpularProducts).subscribe { [weak self] event in
            guard let self = self else{ return }
            switch event {
            case let .success(response):
                NetworkParser.shared.parseResponse(data: response.data, returnType: ProductsResponse.self, objResponse: self.productsViewData)
            case let .error(error):
                self.productsViewData.onError(NetWorkError(message: error.localizedDescription))
                print(error)
            }
        }.disposed(by: disposeBag)
    }
}
